var assert = require('assert');
var add = require('./../controllers/add')
describe('Array', function () {
  describe('#indexOf()', function () {
    it('should return -1 when the value is not present', function () {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});

describe("Add function", function () {
    it("Expected sum 3", function () {
      assert.equal(add.sum(1, 2),3);
    });
});

